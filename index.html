<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>JSDoc: Home</title>

    <script src="scripts/prettify/prettify.js"> </script>
    <script src="scripts/prettify/lang-css.js"> </script>
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link type="text/css" rel="stylesheet" href="styles/prettify-tomorrow.css">
    <link type="text/css" rel="stylesheet" href="styles/jsdoc-default.css">
</head>

<body>

<div id="main">

    <h1 class="page-title">Home</h1>

    



    


    <h3> </h3>










    




    <section>
        <article><h1>PlayCanvas Collision Detection v0.1</h1><p>(c) 2016 Mike Talbot / WhyDoIDoIt - MIT LICENSE</p>
<h2>Introduction</h2><p>PlayCanvas is an excellent WebGL game development engine
that is well suited to many tasks.  One of the major limitations
however is its reliance on Ammo.js - an Emscriptem compilation
of the Bullet physics library.</p>
<p>Bullet is a fantastic library, a brilliant piece of work,
but when used in PlayCanvas - especially on mobile
it causes slow frame rates and significant garbage
collection cycles.</p>
<p>New users especially fall foul of relying on the inbuilt
physics only to find their game will never perform
the way they want.</p>
<blockquote>
<p>There are times when you need a physics engine, but
much of the time you can simply get away with collision
detection.  More advanced games require raycasts and full
protection from object penetration too.</p>
</blockquote>
<p>Collision Detection is a library designed to fulfil the
need for robust, fast collision detection and object
separation that is suitable for mobile games built
with PlayCanvas. It fully support raycasting and
compound colliders.</p>
<h2>Features</h2><p>Collision Detection supports the following features:</p>
<ul>
<li>Very high performance broad and narrow phase collision
detection</li>
<li>Raycasts with multiple hits</li>
<li>PlayCanvas collision components of the types: Box,
Sphere, Capsule and Mesh</li>
<li>Script function call backs on the colliding object
and any parent that defines a suitable callback</li>
<li>Collision events</li>
<li>A &quot;Simple&quot; rigid body that prevents object penetration
and allows simple pushing of objects in the world</li>
</ul>
<h2>Demo</h2><p>In this demo you can move around a simple world.  You can push the
cow and the red box, other colliders are fixed.  Touching a collider with the
ray that projects from the front of the player will
turn that object turquoise.</p>
<iframe src="http://playcanv.as/b/ex0bPq7d" width="100%" height="420px" style="border: none" border="none"></iframe>


<table>
<thead>
<tr>
<th>Keys (Player)</th>
<th>&nbsp;</th>
<th>Keys (Camera)</th>
<th>&nbsp;</th>
</tr>
</thead>
<tbody>
<tr>
<td>J</td>
<td>Rotate Left</td>
<td>W</td>
<td>Camera Closer</td>
</tr>
<tr>
<td>I</td>
<td>Move forwards</td>
<td>S</td>
<td>Camera Further</td>
</tr>
<tr>
<td>L</td>
<td>Rotate Right</td>
<td>A</td>
<td>Rotate Camera</td>
</tr>
<tr>
<td>K</td>
<td>Move backwards</td>
<td>D</td>
<td>Rotate Camera</td>
</tr>
<tr>
<td>U</td>
<td>Move left</td>
<td>Q</td>
<td>Ascend</td>
</tr>
<tr>
<td>O</td>
<td>Move right</td>
<td>E</td>
<td>Descend</td>
</tr>
</tbody>
</table>
<p><em>Note: several other buttons exist for rotating on
 other axes.  SPACE will reset to the start position</em></p>
<h2>Installation</h2><p>For just collision detection create a script in your
project called <code>collisiondetection.js</code> and copy in the
file from the repository.</p>
<p>If you wish to also use the simple rigid body do the
same process with the file <code>simplebody.js</code>.</p>
<p>When you have added <code>collisiondetection.js</code> Ammo will be
disabled. If you wish to switch back at any time call
<code>Collision.uninstall()</code>.  You can also call <code>Collision.install()</code>
to re-enable Collision Detection.</p>
<p><a href="https://bitbucket.org/3radicalDev/collision/src">Source Code On BitBucket</a></p>
<h2>Basic Setup</h2><p>Attach a <code>script</code> component to the entities you wish to
detect collisions between.  Add the <code>collisiondetection.js</code>
script to the entity.</p>
<p>Attach a PlayCanvas <code>collision</code> component to the entity and use one of
the supported types.  Box, Sphere, Capsule or Mesh.  Configure
the collision component to fit your entity.</p>
<h4>Configuring Collision Components</h4><p><img src="https://www.dropbox.com/s/lzdzdbqz1nyiygk/Screenshot%202016-03-25%2015.22.29.png?raw=1" style="width: 50%; margin: auto"/></p>
<p>If you are lucky you will be able to simply configure
the collision components to fit your model, however sometimes it
is hard to get a good fit.  In this case you should
create a <em>master</em> entity that will be the thing moved
and controlled and beneath it add the 3D model as a child.
Then make further child entities for each of the collision components
you need to suitable represent your model in the Collision
Detection world.</p>
<p>Each child should be a sibling of the 3D model.
For each child add a <code>collision</code> and a <code>collisiondetection.js</code> script.</p>
<p>You can now position each of the colliders independently
but they will move with the <em>master</em>.</p>
<h4>Fixed Position Items</h4><p>If the item is not going to move - it's scenery, and it cannot
be destroyed then you should set the <code>fixedPosition</code> property on
the collision detection component to <code>true</code>.  This is a
massive performance booster for the environment.</p>
<p>Even when an item can be destroyed or can move you can
still set <code>fixedPosition</code> and when it moves, you can call the <code>move()</code>
method to update its location.  You certainly shouldn't
be moving fixed position items every frame for a long period of time.
You may also switch off the fixedPosition during gameplay.  These
operations are &quot;reasonably costly&quot; but if something
won't move often, might not move at all, or generally
only moves for brief periods <strong>DO</strong> set fixedPosition true.</p>
<h4>Notifying On Collision</h4><p>Your biggest choice is to decide which <code>collisiondetection</code> components
need to <strong>notify</strong> about collisions.  Keeping the number
of notifying components small will be a big performance boost.</p>
<p>Notifying components are <strong>active</strong> and look for collisions.
For instance your player will certainly have any
collision detection components set to <strong>notify</strong>.</p>
<p>You set a property on the <code>collisiondetection</code> component
to indicate whether this entity is going to actively scan
for collisions.  There are no &quot;absolute&quot; rules
about what should notify and what shouldn't.  It's really
down to your game.</p>
<p>Here are a few guidelines.</p>
<p>Things that definitely shouldn't notify:</p>
<ul>
<li>Walls</li>
<li>Non-moving obstacles</li>
<li>Virtual things like &quot;pickups&quot;, tokens etc.</li>
</ul>
<p>Things that definitely should notify:</p>
<ul>
<li>Your player</li>
<li>Anything with a <code>simplebody</code> attached directly or to
a parent</li>
</ul>
<p>Things that might notify:</p>
<ul>
<li>NPCs - though only if they use collision detection and
not raycasts to move (raycasts are usually better) or they
can be pushed by the player or they can &quot;pick things up&quot; or
otherwise interact with the environment.</li>
<li>Bullets/Missiles - though ray casts are normally much more suitable.</li>
</ul>
<h4>Triggers</h4><p>The other thing you might want is an <em>invisible</em> collider that
perhaps triggers an action when the player enters it.  Like a
button floor plate, or an enemy watch region.</p>
<p>You can set <code>trigger</code> to be <code>true</code> to indicate that there
should be no physical interaction between the notifier
and the trigger, but both components will get a callback
to indicate the collision.</p>
<h4>Simple Body</h4><p>To use the simple body component attach the script
to an item that should avoid object penetration.  Create
colliders on that entity or on child entities and ensure
that they are set to <code>notify = true</code>.</p>
<p>Additionally you can configure whether the object should
rotate on collision (use this for things you can push) and which
axes it can use to avoid penetration.  By default objects
do not use the vertical axis to avoid accidentally being
pushed through the floor or into the air.</p>
<p>If the body will interact with other movable items, setting
a mass indicates which items will be harder to move.</p>
<p><img src="https://www.dropbox.com/s/3ypqa0fbyna5p1h/Screenshot%202016-03-25%2015.20.38.png?raw=1" alt="Simple Body"></p>
<h1>Scripting</h1><p>There are three ways you can interact with the collision
detection system in script. There are two ways you
are really likely to use: <em>Script Events</em> and <em>Ray Casts</em>.</p>
<h3>Script Events</h3><p>Whenever anything comes into contact with anything else
there will be a script event fired on the the tree of
objects that contain the collider.</p>
<p><em>Note: the events are discovered at the initialize phase
for performance reasons, if you change the parentage of the
collider you will need to call it's reparent() method. e.g. this.entity.detection.reparent()</em></p>
<p>There are two script events: <code>colliding</code> and <code>trigger</code>.</p>
<h5>Colliding</h5><p><code>colliding</code> is called on the item that has <strong>notify</strong> set as true
it is passed first the other entity, then the proximity information and then a reference
to the entity that was notified.  This means you can
easily handle the event on a parent object while still
knowing which child collider was involved.</p>
<p>Here's a simple script that kills something when it comes
into contact with an entity called 'Enemy'.</p>
<pre class="prettyprint source"><code> pc.script.create('explode', function(app) {

    var Explode = function(entity) {
        this.entity = entity;
    };

    Explode.prototype = {
        colliding: function(other) {
            if(other.name == 'Enemy') {
                //Make some kind of explosion
                this.entity.destroy();
            }
        }
    };

    return Explode;

 });</code></pre><h5>Trigger</h5><p><code>trigger</code> is fired when the notifier enters a certain area.
Triggers do not cause a collision event.</p>
<p>You might use a trigger for:</p>
<ul>
<li>A switch to open a door</li>
<li>The &quot;eyesight&quot; of an enemy</li>
<li>To start certain effects or to commence spawning enemies</li>
</ul>
<p>The trigger event is only sent when one party in the
collision was a trigger.  It is sent to both the trigger
and the notifier.</p>
<pre class="prettyprint source"><code> //Example script to test if the player
 //has entered a volume representing the
 //eye sight of an enemy
 pc.script.create('eyesight', function(app) {

    var Eyesight = function(entity) {
        this.entity = entity;
    };

    Eyesight.prototype = {
        trigger: function(other) {
            if(other.name == 'Player') {
                this.entity.script.movement.goGetHim(other);
            }
        }
    };

    return Eyesight;

 });</code></pre><p>###Ray Casting
Ray casting lets you make queries about what is contained
in the world.  It's a great way to do smart looking
movement scripts. And really useful lasers and bullets.</p>
<p>For example:</p>
<ul>
<li>you can cast a ray from the location of an
object in the direction its moving and see how far it
can go in that direction.</li>
<li>you could ray cast either
side of an NPC to see if there are side corridors to follow.</li>
<li>you can ray cast straight down to see if the player is
on solid ground and what angle that ground is at.</li>
<li>you can raycast your laser beam from it's current location
in the direction it is moving and make it stop exactly
where it hits a target.</li>
<li>you can ensure a fast moving projectile doesn't go
right through a target without hitting it.</li>
</ul>
<p>There are two ray casting functions in Collision Detection. <code>Collision.raycast</code> and <code>Collision.raycastDirection</code>.  The difference is
one requires a start and an end point, the other a start
point, a direction and a distance.</p>
<blockquote>
<p>You should always try to keep ray casts as short as possible
as many long casts can impact performance.  You can also
limit the impact of longer casts by passing an options object
to either call to limit the number of collisions that
should be reported.</p>
</blockquote>
<p>Raycasting returns a list of the things hit (or an empty list). The
list is in the order that the objects were hit by the
ray.</p>
<p>The result is a pseudo array that doesn't support <code>.map</code> and <code>.slice</code>
amongst a few things.  It does support normal <code>for..next</code>
iteration and a <code>.forEach</code> method.  Check out the FastArray
documentation in the API reference for more details. <strong>Do not use
<code>for..in</code> iteration</strong></p>
<blockquote>
<p>Just be aware that to improve memory performance there
are a preallocated 100 lists to be returned.  So long as
you don't store the hit lists or their contents, but
take action on them, you won't notice anything except
the reduction in lag!</p>
<p>If you were to keep lists hanging around for processing they would eventually be overwritten by subsequent casts.</p>
</blockquote>
<p>Here's an example of using a raycast to check if a
player can move in the forwards direction.</p>
<pre class="prettyprint source"><code>pc.script.attribute('speed', 'number', 4);
pc.script.create('moveforwards', function(app) {
    var MoveForwards = function(entity) {
        this.entity = entity;
    };

    MoveForwards.prototype = {
        update: function(dt) {
            //Check if the user wants to move forward
            if(app.keyboard.isPressed(pc.KEY_W)) {
                //Cast a ray ahead
                var hits = Collision.raycastDirection(
                    this.entity.getPosition(),
                    this.entity.forward,
                    2);
                //If the ray hit nothing or the thing it
                //hits is more than 1 away, then move
                if(!hits.length || hits[0].distance > 1)) {
                    this.entity.translate(0,0,dt * (this.speed || 1));
                }
            }
        }
    };

    return MoveForwards;

});</code></pre><p>####Ray Casting Against Meshes
Generally you should avoid having too many mesh colliders
(if any) in your world.  Instead you should try to use
multiple box, sphere and capsule colliders to represent
your game objects.</p>
<p>If you must use meshes then there are two considerations:</p>
<ul>
<li>Normal raycasts are performed against the Convex Hull
of the mesh.  This is basically the mesh inflated until
there is no concavity.  This can be extremely different
for some objects.  The cow in the demo above has some
of these artifacts, a complete level made in Blender
would basically end up like a giant brick!</li>
</ul>
<p>If you can live with that then fine, it's ok for many
smaller objects for sure.  If not then you will need
to consider raycasting just the Mesh.</p>
<p>You can do this by using the <code>.detection</code> on the entity
with the mesh collider to get the collider and then calling
its <code>raycastGeometry</code> method.  This casts the individual
mesh using all of its features.  But it is much slower
than any other cast.</p>
<pre class="prettyprint source"><code>var hits = world.detection.collider.raycastGeometry(
    this.entity.getPosition(),
    this.entity.forward,
    2);</code></pre><p>If you are going to do this, associate the world with
an entity, get a reference to the mesh collider,
then disable the entity so as to not cause
other performance issues.</p>
<p>You can now use the reference to the collider to directly
raycast the geometry.  Be careful with performance when
you do this. 200 enemies all navigating in a world this
way will not be high performance!</p>
<h4>Events</h4><p>You may also subscribe to events on the Collision global
object if you wish to process all collisions.  This is
advance use only.  Please see the API documentation
for details of the events.</p>
<h1>Performance</h1><p>Collision Detection is designed to be super high performance
by employing a few techniques:</p>
<ul>
<li>Grid based broadphase detection - this is the fastest
way except in a few edge cases.</li>
<li>Fast detection algorithms - uses GJK and creates convex
hulls using QuickHull</li>
<li>Never letting go of memory until you tell it to.  This
causes there to be few Garbage Collection cycles that
would impact frame rates and cause glitches.</li>
</ul>
<h2>Demo</h2><p>This demo shows raycasts and collisions of a player against
2500 randomly placed colliders all of which are active.</p>
<p>You run around the world and the player aligns to the floor
and cannot pass by obstacles which are too high. He also
automatically jumps if he detects you jumping off a cliff!</p>
<p>Open the console and type <code>Collision.renderBoundingBoxes = true</code> or
<code>Collision.renderColliders = true</code> to see the currently tested
items after broad phase.</p>
<iframe src="http://playcanv.as/p/cFTbJYYv" width="100%" height="420px" style="border: none" border="none"></iframe>


<table>
<thead>
<tr>
<th>Keys (Player)</th>
<th>&nbsp;</th>
<th>Keys (Camera)</th>
<th>&nbsp;</th>
</tr>
</thead>
<tbody>
<tr>
<td>J</td>
<td>Rotate Left</td>
<td>W</td>
<td>Camera Closer</td>
</tr>
<tr>
<td>I</td>
<td>Move forwards</td>
<td>S</td>
<td>Camera Further</td>
</tr>
<tr>
<td>L</td>
<td>Rotate Right</td>
<td>A</td>
<td>Rotate Camera</td>
</tr>
<tr>
<td>K</td>
<td>Move backwards</td>
<td>D</td>
<td>Rotate Camera</td>
</tr>
<tr>
<td>SPACE</td>
<td>Jump</td>
<td>Q</td>
<td>Ascend</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>E</td>
<td>Descend</td>
</tr>
</tbody>
</table>
<h5>Garbage</h5><p>So one key thing is, when you change the level or move
everything you should call <code>Collision.clearAll()</code> to
allow old objects to be garbage collected.</p>
<h5>Grid</h5><p>Grid detection is fine so long as:</p>
<ul>
<li>Everything isn't in one grid cell</li>
<li>Everything isn't huge in grid coordinates</li>
</ul>
<p>The standard grid is 1.25 world units.  If your objects are all
massively bigger than this, then increase the <code>Collision.cellSize</code>
to match.</p>
<p>If all of your objects are massively smaller than this
then decrease the <code>Collision.cellSize</code>.</p>
<p>Make changes to cellSize early in your code. Preferably
before the <code>initialize</code> function is called on any script.
You can do this by putting the setting outside of the
definition of the script classes prototype.  But you
might have to set <strong>Script Priority</strong> to ensure that
Collision Detection is loaded first.</p>
<p>At present the grid is always in the XZ plane - this means
vertical games would have poorer performance.  For now
if your game is vertical, fake it by making it in the
XZ plane and fiddle with the camera so it looks right.</p>
<h5>Debugging</h5><p>If it's all running badly then probably you've got
colliders that are huge everywhere.  You can see
the bounding boxes of all colliders that are currently
active after broad phase by opening the browser
console and typing. <code>Collision.renderBoundingBoxes = true</code>.</p>
<p>If the screen is a mass of purple, for sure you've probably
scaled colliders incorrectly.</p>
<p>You can also test <code>Collision.renderColliders = true</code> which
will draw all colliders that have passed bounding box tests.</p>
<h4>General Tips</h4><p>The cost of colliders runs basically like this:</p>
<ul>
<li>Notify colliders are the most expensive - they check
the world.</li>
<li>Non-fixed position colliders are next, they write to
the broad phase grid every frame</li>
<li>Fixed colliders are very cheap.  They are held in
a separate grid that is only updated in rare
circumstances (such as calling <code>this.entity.detection.move()</code>)</li>
</ul>
<p>So:</p>
<ul>
<li>Minimise notifiers to things that actually need to
scan for collisions</li>
<li>Nail down everything that won't move to be fixed
position.</li>
<li>Ensure your colliders are well sized and scaled.</li>
<li>Occasionally check that the bounding boxes and
colliders look right by turning on debugging.</li>
</ul>
<h1>Roadmap</h1><p>Collision Detection is a work in progress. Next up:</p>
<ul>
<li><p>EPA algorithm for closest point detection which
will improve the penetration response to make it
smoother.  Especially when penetration vectors are
limited by an objects desire to not move in one or more
axes.</p>
</li>
<li><p>Simple Physics Body - with a tensor and basic physics
abilities - such as stacking and falling.</p>
</li>
</ul>
<p>Future:</p>
<ul>
<li>Friction, joint constraints etc.</li>
</ul>
<h1>Donations</h1><p>This software is free of charge, but if you use it professionally
it would be really nice if you made a donation for it's
development and continued support.</p>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="N2BV2R89JRKB6">
<input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online.">
<img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
</form>


<h1>License</h1><p>PlayCanvas Rapid Collision Detection Library.</p>
<p> Copyright (c) 2016 Mike Talbot (WhyDoIDoIt)</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the &quot;Software&quot;), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to
whom the Software is furnished to do so, subject to the following conditions:</p>
<p> The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.</p>
<p> THE SOFTWARE IS PROVIDED &quot;AS IS&quot;, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
  FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
  DEALINGS IN THE SOFTWARE.</p></article>
    </section>






</div>

<nav>
    <h2><a href="index.html">Home</a></h2><h3>Classes</h3><ul><li><a href="Collision.Capsule.html">Capsule</a></li><li><a href="Collision.Cube.html">Cube</a></li><li><a href="Collision.FastArray.html">FastArray</a></li><li><a href="Collision.Hit.html">Hit</a></li><li><a href="Collision.IterationOptions.html">IterationOptions</a></li><li><a href="Collision.Mesh.html">Mesh</a></li><li><a href="Collision.Proximity.html">Proximity</a></li><li><a href="Collision.Sphere.html">Sphere</a></li><li><a href="pc.html">pc</a></li><li><a href="pc.Entity.html">Entity</a></li><li><a href="pc.Entity_CollisionDetection.html">CollisionDetection</a></li><li><a href="pc.Entity_SimpleBody.html">SimpleBody</a></li><li><a href="pc.Vec3.html">Vec3</a></li></ul><h3>Events</h3><ul><li><a href="Collision.html#.event:colliding">colliding</a></li><li><a href="Collision.html#.event:collisionStart">collisionStart</a></li><li><a href="Collision.html#.event:iterationComplete">iterationComplete</a></li><li><a href="Collision.html#.event:iterationStart">iterationStart</a></li><li><a href="Collision.html#.event:notificationStart">notificationStart</a></li><li><a href="pc.Entity_CollisionDetection.html#event:colliding">colliding</a></li><li><a href="pc.Entity_CollisionDetection.html#event:trigger">trigger</a></li></ul><h3>Namespaces</h3><ul><li><a href="Collision.utilities.html">utilities</a></li></ul><h3>Interfaces</h3><ul><li><a href="Collision.html">Collision</a></li><li><a href="Collision.RayCastOptions.html">RayCastOptions</a></li><li><a href="Collision.Shape.html">Shape</a></li></ul><h3>Global</h3><ul><li><a href="global.html#truthy">truthy</a></li></ul>
</nav>

<br class="clear">

<footer>
    Documentation generated by <a href="https://github.com/jsdoc3/jsdoc">JSDoc 3.4.0</a> on Fri Mar 25 2016 18:16:22 GMT+0000 (GMT)
</footer>

<script> prettyPrint(); </script>
<script src="scripts/linenumber.js"> </script>
</body>
</html>